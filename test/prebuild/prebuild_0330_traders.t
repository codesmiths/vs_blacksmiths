#!/usr/bin/perl

# Test JSON patches for traders and trader JSONs:
#
# * contain valid JSON
# * that buying lists contain only bought items
# * that selling lists contain only sold items
# * that stacksizes cannot be negative (issue #288)
# * that item/block types match

use Test::More;
use warnings;
use strict;
use utf8;

# load the test helper library
use lib 'test/lib';
use VSJSON;

# subroutine definitions
sub test_stock($ $ $ $);

# The global number of tests we did run
my $number_of_tests_run = 0;

use constant TEST_PATH => 'assets/';
use constant DEBUG => 0;

# Step 1: Gather all JSON info
# Step 2: Test JSON patches

my $info = VSJSON::gather_json_info( TEST_PATH );
my $modid = $info->{modid};
like ($modid, qr/^[a-z]+[a-z0-9-]+$/, "modid $modid looks valid");
$number_of_tests_run ++;

# Test patches to be valid
for my $json_file (sort @{ $info->{patches} } )
  {
  next unless $json_file =~ /entities.*-trader-/;

  my $json = VSJSON::parse_json($json_file);
  ok (ref($json), "$json_file has valid JSON");
  $number_of_tests_run ++;

  my $trader_type;
  $trader_type = $1 if $json_file =~ /-(buying|selling).json/;

  isnt ($trader_type, undef, "$json_file is either buying or selling");
  $number_of_tests_run ++;
  next unless $trader_type;

  my $patches = ref($json) eq 'ARRAY' ? $json : [ $json ];

  my $patch_index = 0;
  # for all patches in this file
  for my $patch (@$patches)
    {
    $patch_index ++;

    # we do not need to test the basic patch properties, as these will be
    # tested along with all other patches. So test only trader specific stuff:
    #
    # Example:
    #    "value": {
    #    "code": "bricklayers:glazedplanter-burnt-milky-gold-checker",
    #    "type": "block",
    #        "stacksize": 1,
    #        "stock": {
    #            "avg": 1,
    #            "var": 0
    #        },
    #        "price": {
    #            "avg": 35,
    #            "var": 4
    #         }
    # },

    my $value = $patch->{value};
    # turn the filename into something like "treasurehunter-selling-123"
    my $name = $json_file; $name =~ s/.*-trader-//; $name =~ s/\.json//; $name .= "-$patch_index";

    is (ref($value), 'HASH', "$name: has a valid value entry");
    $number_of_tests_run ++;
    my $code = $value->{code};
    my $type = $value->{type};

    isnt ($code, undef, "$name: code is defined");
    $number_of_tests_run ++;

    # Now that we know we have a defined code, turn the name into "treasurehunter-selling-123-modid:someitem"
    $name .= "-$code";

    # check that the patch path matches the trader type
    isnt ($patch->{path}, undef, "$name: path is defined");
    my $path = $patch->{path} // '';
    like ($path, qr/\/$trader_type\/list/, "$name: path entry matches trader type");
    $number_of_tests_run += 2;

    if ($code =~ /^$modid:/)
      {
      my $rc = VSJSON::check_code($info, $json_file, $name, $code, $type);

      is ($rc, undef, "$json_file:$code is valid");
      $number_of_tests_run ++;
      }

    test_stock(\$number_of_tests_run, $json_file, $name, $value);
    }

  }

# Test trader entities to be valid
for my $json_file (sort @{ $info->{entities} } )
  {
  next unless $json_file =~ /trader-/;

  my $json = VSJSON::parse_json($json_file);
  ok (ref($json), "$json_file has valid JSON");
  $number_of_tests_run ++;

  for my $trader_type (qw/buying selling/)
    {
    # turn the filename into something like "trader-glass:selling-123"
    my $basename = $json_file; $basename .= ":$trader_type";

    my $list = $json->{attributes}->{tradeProps}->{$trader_type}->{list};
    is (ref($list), 'ARRAY', "$basename list is an array");
    $number_of_tests_run ++;

    # Remember all entries seen so we can detect doubles
    my $items_seen = {};

    my $item_index = 0;
    # for all items in this list
    for my $value (@$list)
      {
      my $name = "$basename-$item_index";
      $item_index ++;

      is (ref($value), 'HASH', "$name: has a valid value entry");
      $number_of_tests_run ++;
      my $code = $value->{code};
      my $type = $value->{type};

      isnt ($code, undef, "$name: code is defined");
      $number_of_tests_run ++;

      # Now that we know we have a defined code, turn the name into "treasurehunter-selling-123-modid:someitem"
      $name .= "-$code";

      if (exists $items_seen->{$code})
        {
        is ($items_seen->{$code}, undef, "$json_file:$code seen twice");
        $number_of_tests_run ++;
	next;
	}
      $items_seen->{$code} = $item_index;

      if ($code =~ /^$modid:/)
        {
        my $rc = VSJSON::check_code($info, $json_file, $name, $code, $type);

        is ($rc, undef, "$json_file:$code is valid");
        $number_of_tests_run ++;
        }

      test_stock(\$number_of_tests_run, $json_file, $name, $value);
      }
    }

  }

done_testing( $number_of_tests_run );

# Subroutines

sub test_stock($ $ $ $)
  {
  my ($number_of_tests, $json_file, $name, $value) = @_;

  my $stacksize = $value->{stacksize};
  isnt ($stacksize, undef, "$name: stacksize is defined");
  $stacksize //= 0;
  ok ($stacksize > 0, "$name: stacksize cannot be negative");
  $$number_of_tests += 2;

  # #288 - test stock for negative amounts
  my $avg = $value->{stock}->{avg};
  my $var = $value->{stock}->{var};
  isnt ($avg, undef, "$name: stock avg is defined");
  isnt ($var, undef, "$name: stock var is defined");
  $avg //= 0;
  $var //= 0;
  ok ($avg > 0, "$name: stock avg is > 0");
  ok ($var >= 0, "$name: stock var is >= 0");
  ok (($avg - $var) > 0, "$name: stock amount must be > 0");
  $$number_of_tests += 5;

  # #288 - test price for negative amounts
  $avg = $value->{price}->{avg};
  $var = $value->{price}->{var};
  isnt ($avg, undef, "$name: price avg is defined");
  isnt ($var, undef, "$name: price var is defined");
  $avg //= 0;
  $var //= 0;
  ok ($avg > 0, "$name: price avg is > 0");
  ok ($var >= 0, "$name: price var is >= 0");
  ok (($avg - $var) > 0, "$name: price must be > 0");
  $$number_of_tests += 5;
  }

# End of code

