#!/usr/bin/perl

# Test that clayforming recipes conform to some minimum standards:
#
# * all layers have the same width/height
# * output or returnedStack codes should only reference valid codes

use Test::More;
use warnings;
use strict;
use utf8;

# load the test helper library
use lib 'test/lib';
use VSJSON;

# subroutine definitions
sub check_code($ $ $ $ $);

# The global number of tests we did run
my $number_of_tests_run = 0;

use constant TEST_PATH => 'assets/';
use constant DEBUG => 0;

# Step 1: Gather all JSON info from blocktypes, itemtypes
# Step 2: Test recipes

my $info = VSJSON::gather_json_info( TEST_PATH );
my $modid = $info->{modid};
like ($modid, qr/^[a-z]+[a-z0-9-]+$/, "modid $modid looks valid");
$number_of_tests_run ++;

my $valid_codes = $info->{codes};		# contains all valid "base" codes like "glazedbricks"
my $valid_variants = $info->{variants};		# and all their variants like "glazedbricks-milky-red"

print "# Found " . (scalar keys %$valid_codes) . " codes and " . (scalar keys %$valid_variants) . " variants.\n";

# Test clayforming recipes for valid codes, and pattern size, sorted by name
for my $json_file (sort @{ $info->{clayforming_recipes} } )
  {
  my $json = VSJSON::parse_json($json_file);
  ok (ref($json), "$json_file has valid JSON");
  $number_of_tests_run ++;

  # clayforming recipes are usually only a single recipe, with some exceptions
  # So always turn them into an ARRAY
  my $recipes = ref($json) eq 'HASH' ? [ $json ] : $json;

  is (ref($recipes), 'ARRAY', "$json_file: Clayforming recipes must be an HASH or ARRAY but is " . ref($recipes));
  $number_of_tests_run ++;

  last if ref($recipes) ne 'ARRAY';

  my $recipe_index = 0;
  # for all recipes in this file
  for my $recipe (@$recipes)
    {
    $recipe_index ++;
    my $name = defined $recipe->{code} ? "recipe $recipe->{code}" : "$json_file: recipe $recipe_index";

    # check that the recipe pattern looks valid
    my $pattern = $recipe->{pattern};

    is (ref($pattern), 'ARRAY', "$name: pattern must be an ARRAY");
    $number_of_tests_run ++;
    next unless ref($pattern) eq 'ARRAY';

    my $first_length = -1;
    my $first_width = -1;
    my $layer_index = 0;
    for my $layer (@$pattern)
      {
      is (ref($layer), 'ARRAY', "$name: layer $layer_index must be an ARRAY");
      $number_of_tests_run ++;
      next unless ref($layer) eq 'ARRAY';

      # remember the width of the first layer
      $first_length = scalar @$layer if $first_length == -1;

      is (scalar @$layer, $first_length, "$name: Layer $layer_index has correct number of lines");
      $number_of_tests_run ++;

      my $line_nr = 0;
      # for each line in this layer
      for my $line (@$layer)
	{
	# to speed up things, only run this test if it fails
	if ($line !~ /^[#_ ]+$/)
	  {
          like ($line, qr/^[#_ ]+$/, "$name: Line $line in layer $layer_index is valid");
	  $number_of_tests_run ++;
	  }
	$line_nr ++;
	# remember the length of the first line
	$first_width = length($line) if $first_width == -1;

	# to speed up things, only run this test if it fails
	if (length($line) != $first_width)
	  {
	  is (length($line), $first_width, "$name: Layer $layer_index, line $line_nr has correct length");
	  $number_of_tests_run ++;
	  }
	}
      }

    like ($pattern, qr/[A-Za-z_, ]+/, "$name: Pattern looks valid");
    $number_of_tests_run ++;
    }
  }

done_testing( $number_of_tests_run );

# End of code

#############################################################################
# Subroutines

sub check_code($ $ $ $ $)
  {
  # check that the given code is a valid one
  my ($info, $file, $name, $code, $type) = @_;

  # If the namespace of this code is something other than "$modid:" or "game:",
  # then check if the name space matches the other mod id in the file path
  # for compatibility recipes:
  # 'assets/bricklayers/compatibility/moremetals/recipes/' => $othermod => 'moremetals'
  my $othermod = undef;
  $othermod = $1 if $file =~ /assets\/[a-z0-9]+\/compatibility\/([a-z0-9_-]+)\/recipes/;

  # #355 - compatibility recipes with other mods
  if ($othermod)
    {
    like ($code, qr/^($modid|game|$othermod):/, "$file: $code is in compatibility mod namespace");
    $number_of_tests_run ++;

    # we cannot check the acual code in other mod namespaces, so skip the next test
    return;
    }

  my $rc = VSJSON::check_code($info, $file, $name, $code, $type);

  is ($rc, undef, "$file:$code is valid");
  $number_of_tests_run ++;
  }

