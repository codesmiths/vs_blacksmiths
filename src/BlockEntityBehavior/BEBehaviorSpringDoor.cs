using System.Linq;
using Vintagestory.API.Common;
using Vintagestory.API.Datastructures;
using Vintagestory.API.MathTools;

namespace Blacksmiths
{
    public class BEBehaviorSpringDoor : BlockEntityBehavior
    {
        public bool isTicking = false;
        // zero is default and means "no timer running"
        public float timer = 0;
        public BlockPos Pos;

        private long latch;

        public BEBehaviorSpringDoor(BlockEntity blockentity) : base(blockentity)
        {
            Pos = blockentity.Pos;
        }

        protected Block GetBlock() {
            if (Api == null) {
                return null;
            }
            return Api.World.BlockAccessor.GetBlock(Pos);
        }
        
        public override void Initialize(ICoreAPI api, JsonObject properties)
        {
            base.Initialize(api, properties);

            // to start ticking after load
            if (isTicking) {
                initTicking(true);
            }
        }

        public void StartClosing(float closeTime)
        {
            // set timer to specified timeout
            timer = closeTime;

            // start server side ticking
            initTicking();
        }

        private void initTicking(bool initAnyway = false)
        {
            // override to reregister even if already ticking according to bool
            if (isTicking && !initAnyway) {
                return;
            }

            if (Api.Side == EnumAppSide.Server) {
                // register ticking method, return is latch to unregister again
                latch = Blockentity.RegisterGameTickListener(OnServerTick, 250);
                isTicking = true;
            }
        }

        private void stopTicking()
        {
            if (!isTicking) {
                return;
            }

            if (Api.Side == EnumAppSide.Server)
            {
                // unregister ticking method using latch
                Blockentity.UnregisterGameTickListener(latch);
                isTicking = false;
            }
        }

        private void OnServerTick(float dt)
        {
            // early return for default value
            if (timer == 0) {
                return;
            }

	    // decrease timer by delta time if timer is active and > dt
            if (timer > dt) {
                timer -= dt;
            }
	    else
	    {
		// timer <= dt means it would result in exactly 0, or <0, so the
		// time is up, trigger closing and reset timer to default value
                timer = 0;
                Close();
            }
        }

        private void Close()
        {
            stopTicking();

            if (GetBlock() is BlockSpringDoor doorBlock) {
                doorBlock.Close(Api.World, null, Pos); // byPlayer would only be needed for breakable doors (log). currently springdoors do not break on use
            }
        }

        ~BEBehaviorSpringDoor()
        {
            stopTicking();
        }

        public override void FromTreeAttributes(ITreeAttribute tree, IWorldAccessor worldAccessForResolve)
        {
            base.FromTreeAttributes(tree, worldAccessForResolve);
            timer = tree.GetFloat("timer");
            isTicking = tree.GetBool("ticking");

            Pos = new BlockPos(tree.GetInt("posX"), tree.GetInt("posY"), tree.GetInt("posZ"));
        }

        public override void ToTreeAttributes(ITreeAttribute tree)
        {
            base.ToTreeAttributes(tree);
            tree.SetFloat("timer", timer);
            tree.SetBool("ticking", isTicking);

            tree.SetInt("posX", Pos.X);
            tree.SetInt("posY", Pos.Y);
            tree.SetInt("posZ", Pos.Z);
        }
    }
}
