using System.Linq;
using Vintagestory.API.Common;
using Vintagestory.API.MathTools;
using Vintagestory.GameContent;

namespace Blacksmiths
{
    public class BlockSpringDoor : BlockDoor
    {
        protected override void Open(IWorldAccessor world, IPlayer byPlayer, BlockPos position)
        {
            base.Open(world, byPlayer, position);

            // only trigger auto-closing when door has been opened
            if (!IsOpened()) {
                if (world.BlockAccessor.GetBlockEntity(position) is BlockEntitySpringDoor springDoorEntity) {
                    // retrieve SpringDoor Block Entity Behavior
                    var springDoorBehavior = springDoorEntity.Behaviors.Where((beh) => {
                        return beh is BEBehaviorSpringDoor;
                    }).First() as BEBehaviorSpringDoor;

                    // retrieve close time from block attribute (closeTimeByType)
                    var closeTime = Attributes["closeTime"].AsFloat(0);
                    // start process of closing door after timeout
                    springDoorBehavior.StartClosing(closeTime);
                }
            }
        }

        public void Close(IWorldAccessor world, IPlayer byPlayer, BlockPos position)
        {
            // only close door if it is open
            if (IsOpened()) {
                Open(world, byPlayer, position);

                // @Tels you may add another sound to the blocks attributes for custom close sound, for example a *sproing* sound
                var triggerSound = Attributes["closeSound"].AsString();
		if (triggerSound is null)
		    {
		    triggerSound = Attributes["triggerSound"].AsString();
		    }
		if (!(triggerSound is null))
		    {
		    world.PlaySoundAt(new AssetLocation(triggerSound), position.X + 0.5, position.Y + 0.5, position.Z + 0.5, null);
		    }
            }
        }

        public override void OnBlockBroken(IWorldAccessor world, BlockPos pos, IPlayer byPlayer, float dropQuantityMultiplier = 1)
        {
            base.OnBlockBroken(world, pos, byPlayer, dropQuantityMultiplier);

            // remove blockEntity when block is broken
            if (world.BlockAccessor.GetBlockEntity(pos) is BlockEntitySpringDoor springDoorEntity) {
                world.BlockAccessor.RemoveBlockEntity(pos);
            }
        }
    }
}
