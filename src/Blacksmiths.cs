﻿using Vintagestory.API.Common;
using System.Linq;
using Vintagestory.API.Server;

namespace Blacksmiths
{
    public class Blacksmiths : ModSystem
    {
        public override bool ShouldLoad(EnumAppSide forSide)
        {
            return true;
        }

        public override void StartServerSide(ICoreServerAPI api)
        {
            api.Logger.Debug("[Blacksmiths] StartServer");
            base.Start(api);
        }

        public override void Start(ICoreAPI api)
        {
            api.Logger.Debug("[Blacksmiths] Start");
            base.Start(api);

            api.RegisterBlockClass("BlockSpringDoor", typeof(BlockSpringDoor));
            api.RegisterBlockEntityClass("BlockEntitySpringDoor", typeof(BlockEntitySpringDoor));
            api.RegisterBlockEntityBehaviorClass("BlockEntityBehaviorSpringDoor", typeof(BEBehaviorSpringDoor));
        }
    }
}
