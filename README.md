# Blacksmiths

"Heavy metal works."

  <u>**This is an alpha release and work in progress!**</u>

This Vintage Story mod is all about working hot metal.

This mod adds new recipes for easier metal automation crafting, as well as recycling of certain metal blocks and items.

You can find more information and pretty pictures at our [Wiki](https://gitlab.com/codesmiths/vs_blacksmiths/-/wikis/home).

We hope you enjoy our work and if you want to support us, please consider becoming a [Patron](https://www.patreon.com/telsandphiwa).

~Phiwa & Tels

# Download

* The mod From [Official Vintage Story ModDB](https://mods.vintagestory.at/show/mod/blacksmiths)
* The source code can be found on [Gitlab](https://gitlab.com/codesmiths/vs_blacksmiths/-/releases)

# Installation

This mod should work in existing worlds. If in doubt, please create a new world.

  <u>**Make a backup of your savegame and world before trying this mod!**</u>

# Features

## Autoclosing doors

* Craft springs in three strengths (bronze, iron or steel) and attach them to doors to make them autoclose after a certain time, depending on the strenght of the spring attached.

## Easier Crafting

* Craft chutes and chute sections from copper plates

## Recycling

* Recycle chute sections, hoppers and metal blocks back into plates
* Recycle chutes back into chute sections

# Languages

* English (100%)
* German (100%)

If you would like to translate this mod into other languages, please [contact us](https://gitlab.com/codesmiths/vs_blacksmiths/-/wikis/Contact).

# Changes and Roadmap

Please see the [Changelog](Changelog) for the changes in the latest release.

The full [Roadmap](https://gitlab.com/codesmiths/vs_blacksmiths/-/wikis/Roadmap) page contains all the planned or possible features.

If you have any ideas, or know about features that already done in another mod, then we
love [to hear from you](https://gitlab.com/codesmiths/vs_blacksmiths/-/wikis/Contact).

# Signature key

All our releases are signed using PGP (GnuPG) and their integrity can be verified by using the public key as published
[on the wiki](https://gitlab.com/codesmiths/vs_blacksmiths/-/wikis/Signing-key).

