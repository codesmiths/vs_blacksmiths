
# v0.3.2 - 2023-04-20 - Upgrades

## Bug Fixes

* #50 - Recompile for 1.18.x

# v0.3.1 - 2023-03-18 - Reflections

## Bug Fixes

* #47 - thin polished sheet blocks were not shiny
* #47 - fix sound issues for sheets and sheet blocks

# v0.3.0 - 2023-02-20 - Reflections

## Bug Fixes

* Update the mod to v1.17.x and specifically to 1.17.11
* #45 - Return linen when polishing sheets
* #44 - Update testsuite
* #42 - Rename sheetings to "thin sheet"
* #41 - Fix smelting properties
* #40 - Make patches server-side only
* #32 - Use reflectiveMode (fix reflections for 1.17)
* #30 - Add missing description to smithing sheets

## New features

* #35 - Add grid recipe sheet => thin sheet

# v0.2.0 - 2022-03-03 - Between the Sheets

## Bug Fixes

* #24 - Update the testsuite
* #21 - Adapt the mod for VS 1.16
* #15 - Update spanish translation
* #19 - Fix rough wooden doors being able to turn into regular spring doors

## New features

### Issue #25 - Metal sheets

* Make vanilla metal sheets craftable
* Enable vanilla metal sheets via helvehammer, too
* Add a thinner metal sheeting and enable it to be helvehammered
* Change vanilla recipe for metal blocks to require a hammer, soldering iron and tin solder
* Add metal sheet blocks made from planks and sheetings.
* Allow polishing of meal sheetings to create dull and reflective variants

# v0.1.0 - 2021-10-15 - The Springy Release

## New features

* #13 - Add autoclosing iron doors (with springs)
* #9 - Add autoclosing wooden doors (with springs)
* #8 - Smith metal springs in three strenghts: bronze, iron and steel

## Translations

* #14 - Add Spanish translation (Thanx **Darce**!)

# v0.0.1 - 2021-08-16 - First Spark

## New features

* #4 - Recycle chutes into chute sections; chute sections and hoppers into copper plates
* #3 - Craft chutes and chute sections from metal plates

