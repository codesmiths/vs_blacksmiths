#!/bin/sh

mkdir -p dist/
if [ $(cat modinfo.json | jq -r .type) = 'code' ]
then
  echo "Building code:"
  mkdir -p distDebug/
  dotnet build
else
  echo "Mod is content only, skipping code build."
fi
