This mod would have not been possible with the help of countless other people. We stand on the shoulders of giants!

Thank you,

~Tels & Phiwa

# Contributors

Without you, this mod would not have been possible - thank you!

### Code

* Autoclosing behaviour: Asraiel

### Translations

* French: **Drakker**
* German: **Tels**, **Phiwa**
* Spanish: **Darce**

### Testing, Feedback and Bugreporting

* **Dampus**

## Thank you

